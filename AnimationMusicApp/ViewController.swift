//
//  ViewController.swift
//  AnimationMusicApp
//
//  Created by Marko Živko on 02/08/2019.
//  Copyright © 2019 Marko Živko. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    
    @IBOutlet var albumImageView: UIImageView!
    @IBOutlet var reverseBackground: UIView!
    @IBOutlet var reverseButton: UIButton!
    @IBOutlet var playPauseBackground: UIView!
    @IBOutlet var playPauseButton: UIButton!
    @IBOutlet var forwardBackground: UIView!
    @IBOutlet var forwardButton: UIButton!
    
    //MARK: part2
    //bool property to keep track wheter the song is playing or not
    var isPlaying: Bool = true{
        
        didSet{
            if isPlaying{
                playPauseButton.setImage(UIImage(named: "pause")!, for: .normal)
            }else{
                playPauseButton.setImage(UIImage(named: "play")!, for: .normal)
            }
        }
        
    }
    
    //MARK: part1
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        let frameHeight = reverseBackground.frame.size.height
        
        reverseBackground.layer.cornerRadius = 0.5 * frameHeight
        reverseBackground.clipsToBounds = true
        reverseBackground.alpha = 0.0
        
        playPauseBackground.layer.cornerRadius = 0.5 * frameHeight
        playPauseBackground.clipsToBounds = true
        playPauseBackground.alpha = 0.0
        
        forwardBackground.layer.cornerRadius = 0.5 * frameHeight
        forwardBackground.clipsToBounds = true
        forwardBackground.alpha = 0.0
    }
    
    
    //MARK: part3 - animating button on state change
    @IBAction func playPauseButtonTapped(_ sender: UIButton) {
        
        if isPlaying{
            UIView.animate(withDuration: 0.5) {
                self.albumImageView.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
            }
        }else{
            UIView.animate(withDuration: 0.5) {
                self.albumImageView.transform = CGAffineTransform.identity
            }
        }
        
        isPlaying.toggle()
        
    }
    
    //MARK: part4 adding touched up and touched down actions
    
    @IBAction func touchedUpInside(_ sender: UIButton) {
        
        var buttonBackground: UIView
        
        switch sender{
        case reverseButton:
            buttonBackground = reverseBackground
        case playPauseButton:
            buttonBackground = playPauseBackground
        case forwardButton:
            buttonBackground = forwardBackground
        default:
            return
        }
        
        UIView.animate(withDuration: 0.25, animations:  {
            buttonBackground.alpha = 0.0
            buttonBackground.transform = CGAffineTransform(scaleX: 1.2, y: 1.2)
            sender.transform = CGAffineTransform.identity
        }) { (_) in
            buttonBackground.transform = CGAffineTransform.identity
        }
    }
    
    
    @IBAction func touchedDown(_ sender: UIButton) {

        let buttonBackground: UIView
        
        switch sender {
        case reverseButton:
            buttonBackground = reverseBackground
        case playPauseButton:
            buttonBackground = playPauseBackground
        case forwardButton:
            buttonBackground = forwardBackground
        default:
            return
        }
        
        UIView.animate(withDuration: 0.25) {
            buttonBackground.alpha = 0.3
            sender.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        }
        
    }
}

